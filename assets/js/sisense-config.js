 
//	Define the Sisense objects to load	
window.sisenseApp = {
	"savedInstance": null,
	"activeDashboard": null,
	"activeDashboardId": null,
	"settings":{
		"serverUrl": "http://examples.sisense.com",
		"scriptId": "sisenseJsLibrary",
		"dateFormatString": "YYYY-MM-DD",
		"postMessageEvent": "sisenseMessage",
		"hiddenCardClass": "sisense-hidden",
		"iframeQueryFormat": "?embed=true&r=false",
		"testApiEndpoint": "/api/v1/plugins/get_info?returnFile=true"
	},
	"dashboards": {
		"dashboard.html": {
			"settings": {
				"titleDiv": "dashboardTitle",
				"titleText": "Summary Dashboard",
				"pulseEnabled": false,
				"pulseListDiv": "pulseList",
				"pulseCountDiv": "pulseCount"
			},
			"filters":{
				"show": true,
				"divId": "sisenseFilters",
				"dashboardId": "57291b9c47ac823429000060",
				"loaded": false,
				"isCustomRender": false
			},
			"widgets": {
				"57291b9c47ac823429000078" : {
					"divId": "a1",
					"nonStandard": true,
					"title": {
						"enabled": false,
						"divId": null,
						"color": null
					},
					"css": {
						"height": "100px",
						"width": "35%"
					}
				},
				"57291b9c47ac823429000073" : {
					"divId": "a2",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": null,
						"color": "orange"
					},
					"animation": "magictime spaceInLeft",
					"animationDelay": 0,
					"css": {
						"height": "300px"
					}
				},
				"57291b9c47ac823429000075" : {
					"divId": "a3",
					"nonStandard": true,
					"title": {
						"enabled": false,
						"divId": null,
						"color": null
					},
					"css": {
						"height": "100px",
						"width": "35%"
					}
				},
				"57291b9c47ac823429000064" : {
					"divId": "a4",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": null,
						"color": "orange"
					},
					"animation": "magictime spaceInRight",
					"animationDelay": 0,
					"css": {
						"height": "300px"
					}
				},
				"57291b9c47ac82342900006a" : {
					"divId": "b1",
					"nonStandard": false,
					"title": {
						"enabled": true,
						"divId": null,
						"color": "orange"
					},
					"animation": "magictime vanishIn",
					"animationDelay": 1000,
					"css": {
						"height": "200px"
					}
				},
				"57291b9c47ac823429000077" : {
					"divId": "b2",
					"nonStandard": false,
					"title": {
						"enabled": true,
						"divId": null,
						"color": "orange"
					},
					"animation": "magictime vanishIn",
					"animationDelay": 1000,
					"css": {
						"height": "200px"
					}
				},
				"57291b9c47ac823429000076" : {
					"divId": "c1",
					"nonStandard": false,
					"title": {
						"enabled": true,
						"divId": null,
						"color": "red"
					},
					"animation": "magictime vanishIn",
					"animationDelay": 1000,
					"css": {
						"height": "352px"
					}
				},
				"57291b9c47ac82342900006c" : {
					"divId": "c2",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": "c2title",
						"color": "red"
					},
					"animation": "magictime vanishIn",
					"animationDelay": 1000,
					"css": {
						"height": "300px"
					}
				},
				"57291b9c47ac823429000068" : {
					"divId": "c3",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": "c3title",
						"color": "red"
					},
					"css": {
						"height": "300px"
					}
				}
			}
		},
		"clicks.html": {
			"settings": {
				"titleDiv": "dashboardTitle",
				"titleText": "Clicks Dashboard",
				"pulseEnabled": false,
				"pulseListDiv": "pulseList",
				"pulseCountDiv": "pulseCount"
			},
			"filters":{
				"show": true,
				"divId": "sisenseFilters",
				"dashboardId": "57291b9c47ac823429000060",
				"loaded": false,
				"isCustomRender": false
			},
			"widgets": {
				"57171b7b57c20a341800015c" : {
					"divId": "a1",
					"nonStandard": false,
					"title": {
						"enabled": false,
						"divId": null,
						"color": null
					},
					"css": {
						"height": "150px"
					}
				},
				"57171b7b57c20a341800015b" : {
					"divId": "a2",
					"nonStandard": false,
					"title": {
						"enabled": false,
						"divId": null,
						"color": null
					},
					"css": {
						"height": "150px"
					}
				},
				"57171b7b57c20a3418000154" : {
					"divId": "a3",
					"nonStandard": false,
					"title": {
						"enabled": false,
						"divId": null,
						"color": null
					},
					"css": {
						"height": "150px"
					}
				},
				"57171b7b57c20a3418000153" : {
					"divId": "b1",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": "b1title",
						"color": "blue"
					},
					"css": {
						"height": "450px"
					}
				},
				"575eeed3e2c12924510000a3" : {
					"divId": "b2",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": "b2title",
						"color": "blue",
						"customText":"table"
					},
					"css": {
						"height": "400px"
					}
				},
				"57171b7b57c20a3418000156" : {
					"divId": "b3",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": "b3title",
						"color": "blue"
					},
					"css": {
						"height": "450px"
					}
				},
				"57171b7b57c20a3418000151" : {
					"divId": "b4",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": "b4title",
						"color": "blue",
						"customText":"table"
					},
					"css": {
						"height": "400px"
					}
				},
				"57171b7b57c20a341800015a" : {
					"divId": "b5",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": "b5title",
						"color": "blue"
					},
					"css": {
						"height": "450px"
					}
				},
				"57171b7b57c20a3418000159" : {
					"divId": "b6",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": "b6title",
						"color": "blue",
						"customText":"table"
					},
					"css": {
						"height": "400px"
					}
				}
			}
		},
		"impression.html": {
			"settings": {
				"titleDiv": "dashboardTitle",
				"titleText": "Impressions Dashboard",
				"pulseEnabled": false,
				"pulseListDiv": "pulseList",
				"pulseCountDiv": "pulseCount"
			},
			"filters":{
				"show": false,
				"divId": null,
				"dashboardId": null,
				"loaded": false
			},
			"customFilters": {
				"datasource": {
					"title": "Online Marketing",
					"address": "LocalHost"
				},
				"filters":[
				{
					"dimension":"[Final.Lead source]",
					"datatype":"text",
					"title": "Lead Source",
					"divId": "filterDropdown1",
					"labelDivId": "filterDropdown1Label",
					"type": "dropdown",
					"color": "green",
					"noSelectionText": "All"
				},{
					"dimension":"[Final.Target Type]",
					"datatype":"text",
					"title": "Acquisition Targets",
					"divId": "filterToggle1",
					"labelDivId": "filterToggle1Label",
					"type": "toggle",
					"color": "green",
					"members": ["Acquisition"]
				},{
					"dimension":"[Final.Date (Calendar)]",
					"datatype":"datetime",
					"level": "days",
					"title": "Date Range",
					"divId": "filterSlider1",
					"labelDivId": "filterSlider1Label",
					"type": "slider",
					"color": "green",
					"momentFormatString": "MMM D, YYYY"
				}
				]
			},
			"widgets": {
				"57171b8657c20a341800016f" : {
					"divId": "a1",
					"nonStandard": false,
					"title": {
						"enabled": false,
						"divId": null,
						"color": null
					},
					"css": {
						"height": "150px"
					}
				},
				"57171b8657c20a3418000178" : {
					"divId": "a2",
					"nonStandard": false,
					"title": {
						"enabled": false,
						"divId": null,
						"color": null
					},
					"css": {
						"height": "150px"
					}
				},
				"57171b8657c20a3418000175" : {
					"divId": "a3",
					"nonStandard": false,
					"title": {
						"enabled": false,
						"divId": null,
						"color": null
					},
					"css": {
						"height": "150px"
					}
				},
				"57171b8657c20a3418000176" : {
					"divId": "b1",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": "b1title",
						"color": "green"
					},
					"css": {
						"height": "450px"
					}
				},
				"57171b8657c20a3418000171" : {
					"divId": "b2",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": "b2title",
						"color": "green",
						"customText":"table"
					},
					"css": {
						"height": "400px",
						"width": "400px"
					}
				},
				"57171b8657c20a3418000173" : {
					"divId": "b3",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": "b3title",
						"color": "green"
					},
					"css": {
						"height": "450px"
					}
				},
				"57171b8657c20a3418000170" : {
					"divId": "b4",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": "b4title",
						"color": "green",
						"customText":"table"
					},
					"css": {
						"height": "400px",
						"width": "400px"
					}
				},
				"57171b8657c20a3418000172" : {
					"divId": "b5",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": "b5title",
						"color": "green"
					},
					"css": {
						"height": "450px"
					}
				},
				"57171b8657c20a341800016e" : {
					"divId": "b6",
					"nonStandard": true,
					"title": {
						"enabled": true,
						"divId": "b6title",
						"color": "green",
						"customText":"table"
					},
					"css": {
						"height": "400px"
					}
				}
			}
		}
	}
};
