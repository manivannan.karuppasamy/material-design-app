
//	Define the message name to use
var postMessageEvent = "sisenseMessage";

//	Add event handler
prism.on('apploaded', function(a,b,c){

	//	get the $http service
	var $http = prism.$injector.get('$http');

	//	Get the dashboards for the logged in user
	getDashboards($http);

})

/************************/
/* 	Dashboard List 		*/
/************************/

//	Function to make the API call
function getDashboards($http){

	//	Define query URL
	var url = '/api/v1/dashboards?fields=oid%2Ctitle';

	//	Get the response
	$http.get(url).then(updateParent);
}

// 	Function to notify the container app
function updateParent(response){

	//	Create an object to send to the container app
	var message = {
		"type": postMessageEvent,
		"data": {
			"dashboards": response.data
		}
	}

	//	Send the payload
	sendMessage(message);
}

/************************/
/* 	Utilies			 	*/
/************************/

//	Function to send message to iframe's parent
function sendMessage(message){
	
	//	Only run if working from an iframe
	window.parent.postMessage(message,"*");		
}
